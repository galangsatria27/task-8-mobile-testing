import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Mobile/Login/Login valid credential'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.tap(findTestObject('Object Repository/HomePage/textSauceLabsBackpackHome'), 0)

Mobile.tap(findTestObject('Object Repository/ProductPage/btnAddCart'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/ProductPage/textCart'), 0)

Mobile.verifyElementText(findTestObject('Object Repository/ProductPage/textCart'), '1')

Mobile.tap(findTestObject('ProductPage/textCart'), 0)

Mobile.verifyElementText(findTestObject('Object Repository/ProductPage/textSauceLabsBackpackCart'), productName)

Mobile.tap(findTestObject('Object Repository/ProductPage/btnProceedToCheckout'), 0)

Mobile.setText(findTestObject('Object Repository/ProductPage/fullnameProduct'), 'Galang Satria', 0)

Mobile.setText(findTestObject('Object Repository/ProductPage/jalan1Product'), 'Jalan keren 123', 0)

Mobile.setText(findTestObject('Object Repository/ProductPage/jalan2Product'), 'Jl 123', 0)

Mobile.hideKeyboard()

Mobile.setText(findTestObject('Object Repository/ProductPage/kotaProduct'), 'Surabaya', 0)

Mobile.setText(findTestObject('Object Repository/ProductPage/provinsiProduct'), 'Jawa Timur', 0)

Mobile.setText(findTestObject('Object Repository/ProductPage/kodePosProduct'), '68712', 0)

Mobile.setText(findTestObject('Object Repository/ProductPage/negaraProduct'), 'Indo', 0)

Mobile.closeApplication()

